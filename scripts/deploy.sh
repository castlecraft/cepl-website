#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls --namespace cepl -q | grep cepl-web)
if [ "$CHECK_WEB" = "cepl-web" ]
then
    echo "Updating existing cepl-web . . ."
    helm upgrade cepl-web \
        --namespace cepl \
        --reuse-values \
        helm-chart/cepl-web
fi
